<?php
// include database and object files
include_once 'config/database.php';
include_once 'objects/category.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

$category = new Category($db);

$parents = $category->getCategoriesWithProductCount();


// set page header
$page_title = "Category With Child Page";
include_once "layout_header.php";
?>

    <ul class="list-group">
        <?php
        while ($row_category = $parents->fetch(PDO::FETCH_ASSOC)) {
            echo "<li class=\"list-group-item\">" . $row_category['Name'] . " (" . $row_category['items'] . ")";
            $categories = $category->getCategoriesWithChildProductCount($row_category['Id']);
            while ($row = $categories->fetch(PDO::FETCH_ASSOC)) {
                echo "<ul class=\"list-group\">";
                echo "<li class=\"list-group-item\">" . $row['Name'] . " (" . $row['items'] . ")" . "</li>";
                echo "</ul>";
            }
            echo "</li>";
        }
        ?>
    </ul>

<?php
// set page footer
include_once "layout_footer.php";
