<?php
// include database and object files
include_once 'config/database.php';
include_once 'objects/category.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

$category = new Category($db);

$categories = $category->getCategoriesWithProductCount();

// set page header
$page_title = "Category Page";
include_once "layout_header.php";
?>

<table class="table table-bordered">
    <thead>
    <tr>
        <th>Category Name</th>
        <th>Total Items</th>
    </tr>
    </thead>
    <tbody>
    <?php
    while ($row_category = $categories->fetch(PDO::FETCH_ASSOC)) {
        echo "<tr>";
        echo "<td>" . $row_category['Name'] . "</td>";
        echo "<td>" . $row_category['items'] . "</td>";
        echo "</tr>";
    }
    ?>
    </tbody>
</table>

<?php
// set page footer
include_once "layout_footer.php";
