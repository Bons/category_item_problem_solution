<?php

class Category
{

    // database connection and table name
    private $conn;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    // get all categories with product count
    function getCategoriesWithProductCount()
    {
        //select all data
        $query = "select
  `category`.`Id`,
  `category`.`Name`,
  count(p.categoryId) as items
from
  `category`
  inner join `Item_category_relations` as `p` on `category`.`Id` = `p`.`categoryId`
group by
  `category`.`Id`
order by
  `items` desc";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    // get all categories with child and product count
    function getCategoriesWithChildProductCount($id)
    {
        $query = "select
  `category`.`Id`,
  `category`.`Name`,
  count(p.categoryId) as items
from
  `category`
  inner join `catetory_relations` as `c` on `category`.`Id` = `c`.`categoryId`
  inner join `Item_category_relations` as `p` on `category`.`Id` = `p`.`categoryId`
where
  `c`.`ParentcategoryId` = " . $id . "
group by
  `category`.`Id`
order by
  `items` desc";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

}